var selectedRow = null

function onFormSubmit() {
    if (validate()) {
        var formData = readFormData();
        if (selectedRow == null)
            insertNewRecord(formData);
        else
            updateRecord(formData);
        resetForm();
    }
}

function readFormData() {
    var formData = {};
    formData["judul"] = document.getElementById("judul").value;
    formData["penulis"] = document.getElementById("penulis").value;
    formData["tahun"] = document.getElementById("tahun").value;
    return formData;
}

function insertNewRecord(data) {
    var table = document.getElementById("list_buku").getElementsByTagName('tbody')[0];
    var newRow = table.insertRow(table.length);
    cell1 = newRow.insertCell(0);
    cell1.innerHTML = data.judul;;
    cell2 = newRow.insertCell(1);
    cell2.innerHTML = data.penulis;
    cell3 = newRow.insertCell(2);
    cell3.innerHTML = data.tahun;
    cell4 = newRow.insertCell(3);
    cell4.innerHTML = `<a onClick="onEdit(this)">Edit</a>
                       <a onClick="onDelete(this)">Delete</a>`;
}

function resetForm() {
    document.getElementById("judul").value = "";
    document.getElementById("penulis").value = "";
    document.getElementById("tahun").value = "";
    selectedRow = null;
}

function onEdit(td) {
    selectedRow = td.parentElement.parentElement;
    document.getElementById("judul").value = selectedRow.cells[0].innerHTML;
    document.getElementById("penulis").value = selectedRow.cells[1].innerHTML;
    document.getElementById("tahun").value = selectedRow.cells[2].innerHTML;
}
function updateRecord(formData) {
    selectedRow.cells[0].innerHTML = formData.judul;
    selectedRow.cells[1].innerHTML = formData.penulis;
    selectedRow.cells[2].innerHTML = formData.tahun;
}

function onDelete(td) {
    if (confirm('Are you sure to delete this record ?')) {
        row = td.parentElement.parentElement;
        document.getElementById("list_buku").deleteRow(row.rowIndex);
        resetForm();
    }
}
function validate() {
    isValid = true;
    if (document.getElementById("judul").value == "" || document.getElementById("penulis").value == "" || document.getElementById("tahun").value == "") {
        isValid = false;
        document.getElementById("judulValidationError").classList.remove("hide");
    } else {
        isValid = true;
        if (!document.getElementById("judulValidationError").classList.contains("hide"))
            document.getElementById("judulValidationError").classList.add("hide");
    }
    return isValid;
}